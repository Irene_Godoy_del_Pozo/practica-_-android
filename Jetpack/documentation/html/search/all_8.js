var searchData=
[
  ['sample_5fscene_14',['Sample_Scene',['../classexample_1_1_sample___scene.html',1,'example::Sample_Scene'],['../classexample_1_1_sample___scene.html#a48d0b3e5a6224cb77c6085e76afd1027',1,'example::Sample_Scene::Sample_Scene()']]],
  ['scale_15',['scale',['../classexample_1_1_sprite.html#aec88c7feadc643c51f7d93b25eaa8993',1,'example::Sprite']]],
  ['show_16',['show',['../classexample_1_1_sprite.html#a4e64a861c0a3d35a915bba251e93439c',1,'example::Sprite']]],
  ['size_17',['size',['../classexample_1_1_sprite.html#aa32dcddb5c6c0d61a4f097c0a47958c1',1,'example::Sprite']]],
  ['speed_18',['speed',['../classexample_1_1_sprite.html#a6c1033f0005bacb94f9dcf980dc837dd',1,'example::Sprite']]],
  ['sprite_19',['Sprite',['../classexample_1_1_sprite.html',1,'example::Sprite'],['../classexample_1_1_sprite.html#a56265a263dbea00cd232a80323f49c54',1,'example::Sprite::Sprite()']]],
  ['suspend_20',['suspend',['../classexample_1_1_game___scene.html#aa8ea9344a39dbfe0ddabbdf10d5528c7',1,'example::Game_Scene::suspend()'],['../classexample_1_1_intro___scene.html#a45a2c00481bca87d43c1270f2a2cf892',1,'example::Intro_Scene::suspend()'],['../classexample_1_1_menu___scene.html#a44d097e87bdb7430d5a00c7e77a00aca',1,'example::Menu_Scene::suspend()'],['../classexample_1_1_sample___scene.html#acb7424d6b3f40f9b61df03641bc0e79c',1,'example::Sample_Scene::suspend()']]]
];
