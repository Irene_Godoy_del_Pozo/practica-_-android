

#include "Sample_Scene.hpp"
#include "Game_Scene.hpp"
#include <basics/Accelerometer>
#include <basics/Canvas>
#include <basics/Director>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

using namespace basics;
using namespace std;

namespace example
{

    Sample_Scene::Sample_Scene()
    {
        state         = LOADING;
        suspended     = true;
        opciones=NONE;
        canvas_width  = 1280;
        canvas_height =  720;
        initialize();


    }

    void Sample_Scene::handle (basics::Event & event)
    {
        if (state == READY)                     // Se descartan los eventos cuando la escena está LOADING
        {
            switch (event.id)
            {
                case ID(touch-started):

                case ID(touch-moved):
                {
                    // Recogemos el punto tocado por el jugador y se lo pasamos a comprobacion_menu
                    Point2f touch_location = { *event[ID(x)].as< var::Float > (), *event[ID(y)].as< var::Float > () };
                    comprobacion(touch_location);
                    break;
                }


                case ID(touch-ended):           // El usuario deja de tocar la pantalla
                {

                    //Comprobamos si se ha elegido alguna opcion y actuamos en consecuencia
                    if(opciones==PLAY)director.run_scene (shared_ptr< Scene >(new Game_Scene));
                    if(opciones==SALIR)director.stop();

                    break;
                }
            }
        }
    }
    //----------------------------------------------------------------------------------------------
    //Se comprueba si se ha tocado algun texto deseado
    void Sample_Scene::comprobacion (Point2f punto)
    {
        if(opciones==NONE)
        {
            if(     (punto.coordinates.x()>canvas_width*3/5) &&
                    (punto.coordinates.x()<textos[0].coordinates.x())&&
                    (punto.coordinates.y()<canvas_height*5/7) &&
                    (punto.coordinates.y()>textos[0].coordinates.y())
                    ) opciones=PLAY;

            else if(     (punto.coordinates.x()>canvas_width*3/5) &&
                         (punto.coordinates.x()<textos[1].coordinates.x())&&
                         (punto.coordinates.y()<canvas_height*4/7) &&
                         (punto.coordinates.y()>textos[1].coordinates.y())
                    ) opciones=INSTRUCCIONES;
            else if(     (punto.coordinates.x()>canvas_width*3/5) &&
                         (punto.coordinates.x()<textos[2].coordinates.x())&&
                         (punto.coordinates.y()<canvas_height*3/7) &&
                         (punto.coordinates.y()>textos[2].coordinates.y())
                    ) opciones=CREDITS;
            else if(     (punto.coordinates.x()>canvas_width*3/5) &&
                         (punto.coordinates.x()<textos[3].coordinates.x())&&
                         (punto.coordinates.y()<canvas_height*2/7) &&
                         (punto.coordinates.y()>textos[3].coordinates.y())
                    ) opciones=SALIR;

        }
        else if (opciones == INSTRUCCIONES || opciones ==CREDITS) {
            if ((punto.coordinates.x() > canvas_width * 3 / 5) &&
                (punto.coordinates.x() < textos[0].coordinates.x()) &&
                (punto.coordinates.y() < canvas_height * 2 / 7) &&
                (punto.coordinates.y() > textos[0].coordinates.y())
                    )
                opciones = NONE;

        }
    }

//--------------------------------------------------------------------------------------------------
//Durante el loading se creara el context y se cargara la fuente

    void Sample_Scene::update (float time)
    {

        if( state == LOADING)
        {

            if (!font)
            {
                Graphics_Context::Accessor context = director.lock_graphics_context ();

                if (context)
                {
                    font.reset (new Raster_Font("fonts/pixel.fnt", context));


                    state = READY;
                }
            }
        }
    }
//---------------------------------------------------------------------------------------------

//Se actualizan las imagenes
    void Sample_Scene::render (basics::Graphics_Context::Accessor & context)
    {

        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear ();


                if (font)
                {
                    //Si no se ha elegido ninguna opcion
                    if(opciones==NONE )
                    {
                        //Se crea un Text_Layout y se dibuja indicando la posicion deseada
                        Text_Layout name_text(*font, L"JETPIGGY");
                        canvas->draw_text ({ canvas_width*1/5,  canvas_height*4/7}, name_text);

                        // Se dibuja texto de jugar:
                        Text_Layout jugar_text(*font, L"JUGAR");
                        canvas->draw_text ({  canvas_width*3/5, canvas_height*5/7 }, jugar_text );

                        //Se mete en textos el punto de la esquina inferior derecha del texto (se dibuja en la superior izquierda)
                        textos[0]={jugar_text.get_width()+canvas_width*3/5,canvas_height*5/7-jugar_text.get_height()};


                        // Se dibuja texto de instrucciones:

                        Text_Layout instru_text(*font, L"INSTRUCCIONES");
                        canvas->draw_text ({ canvas_width*3/5, canvas_height*4/7 }, instru_text);

                        //Se mete en textos el punto de la esquina inferior derecha del texto (se dibuja en la superior izquierda)
                        textos[1]={instru_text.get_width()+canvas_width*3/5,canvas_height*4/7-instru_text.get_height()};

                        // Se dibuja texto de creditos:

                        Text_Layout credit_text(*font, L"CREDITOS");
                        canvas->draw_text ({ canvas_width*3/5, canvas_height*3/7}, credit_text);

                        //Se mete en textos el punto de la esquina inferior derecha del texto (se dibuja en la superior izquierda)
                        textos[2]={credit_text.get_width()+canvas_width*3/5,canvas_height*3/7-credit_text.get_height()};

                        // Se dibuja texto de salir:

                        Text_Layout salir_text(*font, L"SALIR");
                        canvas->draw_text ({ canvas_width*3/5, canvas_height*2/7 }, salir_text);

                        //Se mete en textos el punto de la esquina inferior derecha del texto (se dibuja en la superior izquierda)
                        textos[3]={salir_text.get_width()+canvas_width*3/5,canvas_height*2/7-salir_text.get_height()};



                        // Se guarda la fecha y la hora actual:

                        std::time_t time_point = std::time (nullptr);
                        std::tm   * time_data  = std::localtime (&time_point);

                        // Se convierte la fecha y la hora a una cadena de tipo wstring:

                        std::wostringstream buffer;

                        // Se convierte la fecha y se termina con un salto de línea:

                        buffer << time_data->tm_mday << "/" << (time_data->tm_mon + 1) << "/" << (time_data->tm_year + 1900) << "       ";

                        // Se convierte la hora asegurándose de que dada número tiene dos dígitos:

                        buffer << std::setfill (L'0');
                        buffer << std::setw (2) << time_data->tm_hour << ":";
                        buffer << std::setw (2) << time_data->tm_min  << ":";
                        buffer << std::setw (2) << time_data->tm_sec;

                        // Se crea el text layout a partir de la cadena creada:

                        Text_Layout time_text(*font, buffer.str ());

                        // Y se dibuja arriba a la izquierda de la pantalla:

                        canvas->draw_text ({ 50.f, canvas_height - 50.f }, time_text, TOP | LEFT);

                    }

                    //Si se eligió la opcion Instrucciones
                    else if(opciones==INSTRUCCIONES)
                    {

                        // Se crean y pintan las instrucciones
                        Text_Layout name_text(*font, L"- PARA MOVER A PIGGY PULSA LA PANTALLA \n\n"
                                                     " - LAS MANZANAS TE DEVOLVERAN ENERGIA \n\n"
                                                     " - COGIENDO MONEDAS CONSEGUIRAS PUNTOS \n\n\n\n"
                                                     " CONSIGUE TODAS LAS MONEDAS QUE PUEDAS \n\n ANTES DE QUEDARTE SIN ENERGIA");

                        canvas->draw_text ({canvas_width*1/7,  canvas_height*6/7 }, name_text);

                        //Opcion de volver atras
                        Text_Layout salir_text(*font, L"VOLVER ATRAS");
                        canvas->draw_text ({ canvas_width*3/5, canvas_height*2/7 }, salir_text);
                        textos[0]={salir_text.get_width()+canvas_width*3/5,canvas_height*2/7-salir_text.get_height()};

                    }
                    //Si se eligió creditos
                    else if (opciones==CREDITS)
                    {
                        //Se crean y pintan los creditos
                        Text_Layout name_text(*font, L" REALIZADO POR : IRENE GODOY DEL POZO \n\n TWITTER: @IRENENOMAS");

                        canvas->draw_text ({canvas_width*1/7,  canvas_height*6/7 }, name_text);

                        //Opcion de volver atras
                        Text_Layout salir_text(*font, L"VOLVER ATRAS");
                        canvas->draw_text ({ canvas_width*3/5, canvas_height*2/7 }, salir_text);
                        textos[0]={salir_text.get_width()+canvas_width*3/5,canvas_height*2/7-salir_text.get_height()};
                    }


                }
            }
        }
    }

}
