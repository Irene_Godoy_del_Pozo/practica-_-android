/*
 * GAME SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Game_Scene.hpp"
#include "Sample_Scene.hpp"

#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>
#include <sstream>

using namespace basics;
using namespace std;

namespace example
{
    // ---------------------------------------------------------------------------------------------
    // ID y ruta de las texturas que se deben cargar para esta escena. La textura con el mensaje de
    // carga está la primera para poder dibujarla cuanto antes:

    Game_Scene::Texture_Data Game_Scene::textures_data[] =
    {
        { ID(loading),    "game-scene/loading.png"        },
        { ID(cielo),       "game-scene/cielo.png" },
        { ID(suelo),       "game-scene/suelo.png"   },
        { ID(coin), "game-scene/coin.png"    },
        { ID(fuel),       "game-scene/fuel.png"           },
        { ID(pj),       "game-scene/personaje.png"           },
    };

    // Para determinar el número de items en el array textures_data, se divide el tamaño en bytes
    // del array completo entre el tamaño en bytes de un item:

    unsigned Game_Scene::textures_count = sizeof(textures_data) / sizeof(Texture_Data);

    Graphics_Context::Accessor context = director.lock_graphics_context ();

    // ---------------------------------------------------------------------------------------------

    Game_Scene::Game_Scene()
    {
        // Se establece la resolución virtual (independiente de la resolución virtual del dispositivo).
        canvas_width  = 1280;
        canvas_height =  720;

        // Se inicia la semilla del generador de números aleatorios:

        srand (unsigned(time(nullptr)));

        // Se inicializan otros atributos:

        initialize ();
    }

    // ---------------------------------------------------------------------------------------------

    bool Game_Scene::initialize ()
    {
        state     = LOADING;
        suspended = true;
        gameplay  = UNINITIALIZED;
        return true;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::suspend ()
    {
        suspended = true;               // Se marca que la escena ha pasado a primer plano
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::resume ()
    {
        suspended = false;              // Se marca que la escena ha pasado a segundo plano
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::handle (Event & event)
    {
        if (state == RUNNING || state ==PAUSE)               // Se descartan los eventos cuando la escena está LOADING
        {
            if (gameplay == WAITING_TO_START)
            {
                start_playing ();           // Se empieza a jugar cuando el usuario toca la pantalla por primera vez
            }
            else switch (event.id)
            {
                case ID(touch-started):     // El usuario toca la pantalla
                {
                    // Recogemos el punto tocado por el jugador y se lo pasamos a comprobacion_menu
                    Point2f touch_location = { *event[ID(x)].as< var::Float > (), *event[ID(y)].as< var::Float > () };
                    comprobacion_menu(touch_location);

                }
                case ID(touch-moved):
                {
                    // Establece que siempre que tenga fuel y se toque la pantalla tiene fuel,ademas de que no se pase del limite superior, subirá
                    if (player.fuel > 0 && (player.pj->get_position_y() < ((5*canvas_height / 7)- player.pj->get_height()))) {
                        player.force = 200.f;
                    } else {
                        player.force = 0;
                    }

                    //La gravedad siempre será la misma , para que en caso de que sea fuerza = 0 , el personaje baje
                    player.gravity = 75.f;
                    player.presed = true;
                    break;

                }

                case ID(touch-ended):       // El usuario deja de tocar la pantalla
                {
                    player.force = 0;
                    player.presed = false;



                    break;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::update (float time)
    {
        if (!suspended) switch (state)
        {
            case LOADING: load_textures  ();     break;
            case RUNNING: run_simulation (time); break;
            case PAUSE:break;
            case ERROR:   break;
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render (Context & context)
    {
        if (!suspended)
        {
            // El canvas se puede haber creado previamente, en cuyo caso solo hay que pedirlo:

            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            // Si no se ha creado previamente, hay que crearlo una vez:

            if (!canvas)
            {
                 canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            // Si el canvas se ha podido obtener o crear, se puede dibujar con él:

            if (canvas)
            {
                canvas->clear ();

                switch (state)
                {
                    case LOADING: render_loading   (*canvas); break;
                    case RUNNING: render_playfield (*canvas); break;
                    case PAUSE: render_pause (*canvas);break;
                    case ERROR:   break;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::load_textures ()
    {
        if (textures.size () < textures_count)          // Si quedan texturas por cargar...
        {
            // Las texturas se cargan y se suben al contexto gráfico, por lo que es necesario disponer
            // de uno:

            Graphics_Context::Accessor context = director.lock_graphics_context ();
            font.reset (new Raster_Font("fonts/pixel.fnt", context));

            if (context)
            {
                // Se carga la siguiente textura (textures.size() indica cuántas llevamos cargadas):

                Texture_Data   & texture_data = textures_data[textures.size ()];
                Texture_Handle & texture      = textures[texture_data.id] = Texture_2D::create (texture_data.id, context, texture_data.path);

                // Se comprueba si la textura se ha podido cargar correctamente:

                if (texture) context->add (texture); else state = ERROR;

            }
        }
        else
        if (timer.get_elapsed_seconds () > 1.f)         // Si las texturas se han cargado muy rápido
        {                                               // se espera un segundo desde el inicio de
            create_sprites ();                          // la carga antes de pasar al juego para que
            restart_game   ();                          // el mensaje de carga no aparezca y desaparezca
            timer.reset();                              // demasiado rápido.
            state = RUNNING;
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::create_sprites ()
    {

        //A continuación se crearan los sprites.


        // SPRITE SUELO
        Sprite_Handle suelo_fondo(new Sprite( textures[ID(suelo)].get () ));

        suelo_fondo->set_anchor   (TOP | LEFT);
        suelo_fondo->set_position ({ 0, canvas_height/5 });
        suelo_fondo->set_scale(3);

        //SPRITE CIELO
        Sprite_Handle cielo_fondo(new Sprite( textures[ID(cielo)].get () ));

        cielo_fondo->set_anchor   (BOTTOM | LEFT);
        cielo_fondo->set_position ({ 0, canvas_height/5 });
        cielo_fondo->set_scale(50);

        // SPRITE FUEL
        Sprite_Handle fuel_handle(new Sprite( textures[ID(fuel)].get () ));

        fuel_handle->set_anchor   (BOTTOM | LEFT);
        fuel_handle->set_scale(0.75f);
        fuel_handle->hide();

        //SPRITE COIN
        Sprite_Handle  coin_handle(new Sprite( textures[ID(coin)].get () ));

        coin_handle->set_anchor   (BOTTOM | LEFT);
        coin_handle->hide();

        //SPRITE PERSONAJE
        Sprite_Handle pj_handle(new Sprite( textures[ID(pj)].get () ));

        pj_handle->set_anchor   (BOTTOM | LEFT);
        pj_handle->set_position ({ player.x, player.y });


        //PUSHBACK TODOS LOS SPRITES

        sprites.push_back ( suelo_fondo  );
        sprites.push_back (cielo_fondo);
        sprites.push_back (fuel_handle);
        sprites.push_back ( coin_handle);
        sprites.push_back (pj_handle);


        //GUARDAMOS LOS PUNTEROS

        floor        =   suelo_fondo.get ();
        fuels.sprite =   fuel_handle.get ();
        coin.sprite  =   coin_handle.get ();
        player.pj    =     pj_handle.get ();

    }

    // ---------------------------------------------------------------------------------------------


    void Game_Scene::restart_game()
    {
        // Inicializacion de los atributos del jugador
        player.x = canvas_width / 5;
        player.y = canvas_height / 5;
        player.speed_y= 0;
        player.gravity = 0; // PARA QUE AL PRINCIPIO NO EMPIECE A BAJAR
        player.force=0;
        player.fuel = 50;
        player.presed = false;
        player.score =0;

        // Inicializacion del fuel

        fuels.is_created = false;
        fuels.speed_x=0;

        // Inicializacion de las monedas

        coin.is_created = false;
        coin.speed_x=0;


        gameplay = WAITING_TO_START;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::start_playing ()
    {
        gameplay = PLAYING;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::run_simulation (float time)
    {
        // Se actualiza el estado de todos los sprites:

        for (auto & sprite : sprites)
        {
            sprite->update (time);
        }

        //Se llama a las funciones de update de fuel, coin y personaje

        update_fuel   ();
        update_coin   ();
        update_user (time);
    }

    // --------------------------------------------UPDATE DE FUEL-------------------------------------------------

    void Game_Scene::update_fuel ()
    {
        /*
         *
         * Si han pasado 4 segundos y el objeto no esta ya en pantalla
         * Se crea un numero aleatorio para determinar la y.
         * Posteriormente se muestra, se actualiza que ha sido creado
         * Por ultimo se establece una velocidad aleatoria constante para esa ocasión
         *
         * */
        if(t_fuel.get_elapsed_seconds() >= 4 && fuels.is_created==false)
        {
            float y = rand () % ((canvas_height*5/7) - (canvas_height/5)) + canvas_height/5;

            fuels.sprite->set_position({canvas_width,y});

            fuels.sprite->show();

            fuels.is_created=true;
            fuels.speed_x=-(rand () % (200 - 80) + 80);


        }
        //Si ya esta creado, se mueve hacia la izquierda en el eje de las x. Además se comprueban limites
        else if (fuels.is_created==true)
        {

            fuels.sprite->set_speed_x(fuels.speed_x);
            check_fuel_collisions();

        }
    }

    //Comprueba si se sale de la pantalla
    void Game_Scene::check_fuel_collisions ()
    {
        if(fuels.sprite->get_position_x()<=0)
        {
            fuels.is_created=false;
            t_fuel.reset();

        }
    }

    //-------------------------------------------------UPDATE COIN----------------------------------------

    void Game_Scene::update_coin ()
    {
        /*
         *
         * Si han pasado 4 segundos y el objeto no esta ya en pantalla
         * Se crea un numero aleatorio para determinar la y.
         * Posteriormente se muestra, se actualiza que ha sido creado
         * Por ultimo se establece una velocidad aleatoria constante para esa ocasión
         *
         * */
        if(t_coin.get_elapsed_seconds() >= 4 && coin.is_created==false)
        {


            float y = rand () % ((canvas_height*5/7) - (canvas_height/5)) + canvas_height/5 ;

            coin.sprite->set_position({canvas_width,y});

            coin.sprite->show();

            coin.is_created=true;
            coin.speed_x=-(rand () % (200 - 80) + 80);


        }
        //Si ya esta creado, se mueve hacia la izquierda en el eje de las x. Además se comprueban limites
        else if (coin.is_created==true)
        {
            coin.sprite->set_speed_x(coin.speed_x);
            check_coin_collisions();
        }
    }
    //Comprueba si se sale de la pantalla
    void Game_Scene::check_coin_collisions ()
    {
        if(coin.sprite->get_position_x()<=0)
        {
            coin.is_created=false;
            t_coin.reset();

        }
    }

    // -------------------------------------------UPDATE PLAYER --------------------------------------------------


    void Game_Scene::update_user (float time)
    {
        // Resta combustible cada  segundo siempre que este presionando la pantalla. Luego se resetea
        if (player.presed == true && t.get_elapsed_seconds() >= 1.f && player.fuel>0)
        {
            player.fuel -= 2;
            t.reset();
        }


        // Movimiento del personaje

        player.pj->set_speed_y(- player.gravity + player.force);


        //Comprobación de colisiones
        check_pj_collisions();
    }



    void Game_Scene::check_pj_collisions ()
    {
        //Si se choca con fuel oculta la manzana, cambia su estado a no creado y suma combustible al jugador
        if(player.pj->intersects(*fuels.sprite))
        {
            fuels.sprite->hide();
            fuels.is_created=false;
            player.fuel += 5;
        }

        //Si se choca con coin oculta la moneda, cambia su estado a no creado y suma puntuación al jugador
        if(player.pj->intersects(*coin.sprite))
        {
            coin.sprite->hide();
            coin.is_created=false;
            player.score += 1;
        }

        //Si choca con el suelo, cambia es estado a LOADING
        if(player.pj->intersects(*floor))
        {
            state = LOADING;

        }


    }

    // ----------------------------------RENDERS-----------------------------------------------------------

    //Render cuando el estado es LOADING
    void Game_Scene::render_loading (Canvas & canvas)
    {
        Texture_2D * loading_texture = textures[ID(loading)].get ();

        if (loading_texture)
        {
            canvas.fill_rectangle
            (
                { canvas_width * .5f, canvas_height * .5f },
                { loading_texture->get_width (), loading_texture->get_height () },
                  loading_texture
            );
        }

    }

    // ---------------------------------------------------------------------------------------------
    // Simplemente se dibujan todos los sprites que conforman la escena.

    void Game_Scene::render_playfield (Canvas & canvas)
    {
        for (auto & sprite : sprites)
        {
            sprite->render (canvas);
        }

        //----------------TEXTOS-----------------

        //Se crea un buffer para concatenar string con la puntuacion del jugador

        std::wostringstream buffer;
        buffer << "SCORE: " << player.score ;

        //Se crea un Text_Layout y se dibuja indicando la posicion deseada

        Text_Layout score_text(*font, buffer.str());
        canvas.draw_text ({  canvas_width*1/5,  canvas_height*6/7 }, score_text);

        //Se crea un buffer para concatenar string con el fuel del jugador

        std::wostringstream buffer_fuel;
        buffer_fuel << "ENERGY: " << player.fuel ;

        //Se crea un Text_Layout y se dibuja indicando la posicion deseada

        Text_Layout fuel_text(*font, buffer_fuel.str());
        canvas.draw_text ({  canvas_width*2/5,  canvas_height*6/7 }, fuel_text);

        //Se crea un Text_Layout y se dibuja indicando la posicion deseada
        Text_Layout opcion_text(*font, L"OPTIONS");
        canvas.draw_text ({  canvas_width*3/5,  canvas_height*6/7 }, opcion_text);
        //Se mete en textos el punto de la esquina inferior derecha del texto (se dibuja en la superior izquierda)
        textos[0]={opcion_text.get_width()+canvas_width*3/5,canvas_height*6/7-opcion_text.get_height()};

        }
//----------------------------------------------------------------------------------------------------

//Render del enu de pausa
    void Game_Scene::render_pause (Canvas & canvas)
    {

        //Se crea un Text_Layout y se dibuja indicando la posicion deseada
        Text_Layout resume_text(*font, L"CONTINUAR");
        canvas.draw_text ({  canvas_width/2,  canvas_height*3/4 }, resume_text);
        //Se mete en textos el punto de la esquina inferior derecha del texto (se dibuja en la superior izquierda)
        textos[0]={resume_text.get_width()+canvas_width/2,canvas_height*3/4-resume_text.get_height()};



    }

    //--------------------------------------------------------------------------------------------------------
    //Se comprueba si se ha tocado algun texto deseado
    void Game_Scene::comprobacion_menu(basics::Point2f punto)
    {
        if(state==PAUSE)
        {
            //Si tocan Continuar, devuelve el state a RUNNING
            if(     (punto.coordinates.x()>canvas_width/2) &&
                    (punto.coordinates.x()<textos[0].coordinates.x())&&
                    (punto.coordinates.y()<canvas_height*3/4) &&
                    (punto.coordinates.y()>textos[0].coordinates.y())
                    ) state=RUNNING;


        }
        if(state==RUNNING)
        {
            //Si tocan Options, devuelve el state a PAUSE
            if(     (punto.coordinates.x()>canvas_width*3/5) &&
                    (punto.coordinates.x()<textos[0].coordinates.x())&&
                    (punto.coordinates.y()<canvas_height*6/7) &&
                    (punto.coordinates.y()>textos[0].coordinates.y())
                    ) state=PAUSE;

        }


    }







    }


