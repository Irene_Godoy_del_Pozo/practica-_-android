

#include <memory>
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Raster_Font>




namespace example
{

    class Sample_Scene : public basics::Scene
    {
    private:

        typedef std::unique_ptr< basics::Raster_Font > Font_Handle;

        /**
             * Representa el estado de la escena en su conjunto.
             */
        enum State
        {
            LOADING,
            READY,
        };

        State    state;                 ///< Estado de la escena.

        basics::Point2f textos[4];      ///<Array de puntos que recogerá el punto de la esquina inferior derecha del texto deseado




        /**
             * Representa las opciones posibles.
             */
        enum Option
        {
            PLAY,
            INSTRUCCIONES,
            SALIR,
            CREDITS,
            NONE
        };
        Option opciones;                ///< Opcion elegida.

        bool        suspended;          ///< true cuando la aplicación está en segundo plano
        unsigned    canvas_width;       ///< Resolución virtual del display
        unsigned    canvas_height;

        Font_Handle font;               ///< Variable que recoge la fuente del texto utilizado



    public:

        /**
             * Solo inicializa los atributos que deben estar inicializados la primera vez, cuando se
             * crea la escena desde cero.
             */
        Sample_Scene();


        /**
             * Este método lo llama Director para conocer la resolución virtual con la que está
             * trabajando la escena.
             * @return Tamaño en coordenadas virtuales que está usando la escena.
             */
        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        /**
             * Aquí se inicializan los atributos que deben restablecerse cada vez que se inicia la escena.
             * @return
             */
        bool initialize () override
        {
            state = LOADING;
            suspended = false;

            return true;
        }
        /**
             * Este método lo invoca Director automáticamente cuando el juego pasa a segundo plano.
             */
        void suspend () override
        {
            suspended = true;
        }

        /**
             * Este método lo invoca Director automáticamente cuando el juego pasa a primer plano.
             */
        void resume () override
        {
            suspended = false;
        }

        /**
             * Este método se invoca automáticamente una vez por fotograma cuando se acumulan
             * eventos dirigidos a la escena.
             */
        void handle (basics::Event & event) override;

        /**
             * Este método se invoca automáticamente una vez por fotograma para que la escena
             * actualize su estado.
             */
        void update (float ) override;
        /**
             * Este método se invoca automáticamente una vez por fotograma para que la escena
             * dibuje su contenido.
             */
        void render (basics::Graphics_Context::Accessor & context) override;
        /**
             * Comprueba si se pulsa alguna opción.
             * @param canvas Referencia al punto que toca el usuario en la pantalla.
             */
        void comprobacion (basics::Point2f  );

    };

}
